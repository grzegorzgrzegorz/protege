package com.passfailerror.gui

import com.passfailerror.dlquery.DLQueryEngine
import groovy.swing.SwingBuilder
import org.semanticweb.HermiT.Reasoner
import org.semanticweb.owlapi.apibinding.OWLManager
import org.semanticweb.owlapi.model.OWLClass
import org.semanticweb.owlapi.model.OWLEntity
import org.semanticweb.owlapi.model.OWLNamedIndividual
import org.semanticweb.owlapi.model.OWLObjectProperty
import org.semanticweb.owlapi.model.OWLOntology
import org.semanticweb.owlapi.model.OWLOntologyManager
import org.semanticweb.owlapi.util.ShortFormProvider
import org.semanticweb.owlapi.util.SimpleShortFormProvider

import java.awt.BorderLayout

/**
 * Created by Magda_i_Grzes on 2017-01-01.
 */
class GuiLayer {


    DLQueryEngine dlQueryEngine;
    OWLOntology ontology;
    def swing;

    def loadOntology(){
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        ontology = manager.loadOntologyFromOntologyDocument(new File(swing.ontology_path.text));
        Reasoner reasoner = new Reasoner(ontology);
        ShortFormProvider shortFormProvider = new SimpleShortFormProvider();
        dlQueryEngine = new DLQueryEngine(reasoner, shortFormProvider)
        swing.current_ontology.text = swing.ontology_path.text
    }

    def askQuestion(){
        def question = swing.question.text
        Set<OWLClass> superClasses = dlQueryEngine.getSuperClasses(question, true);
        Set<OWLClass> equivalentClasses = dlQueryEngine.getEquivalentClasses(question);
        Set<OWLClass> subClasses = dlQueryEngine.getSubClasses(question, true);

        Set<OWLNamedIndividual> individuals = dlQueryEngine.getInstances(question, false);
        String superClassesResponse = "";
        String equivalentClassesResponse = "";
        String subClassesResponse = "";
        String individualsResponse = "";
        if (!superClasses.isEmpty()) {
            for (OWLEntity entity : superClasses) {
                superClassesResponse = superClassesResponse + " | " + new SimpleShortFormProvider().getShortForm(entity);
            }
        }
        if (!equivalentClasses.isEmpty()) {
            for (OWLEntity entity : equivalentClasses) {
                equivalentClassesResponse = equivalentClassesResponse + " | " + new SimpleShortFormProvider().getShortForm(entity);
            }
        }
        if (!subClasses.isEmpty()) {
            for (OWLEntity entity : subClasses) {
                subClassesResponse = subClassesResponse + " | " + new SimpleShortFormProvider().getShortForm(entity);
            }
        }
        if (!individuals.isEmpty()) {
            for (OWLNamedIndividual entity : individuals) {
                individualsResponse = individualsResponse + " | " + entity.getIRI().getFragment();
            }
        }
        swing.current_response.text = "Classes: "+subClassesResponse+"| individuals: "+ individualsResponse;
    }

    public void startSwing() {

        swing = new SwingBuilder().edt {

            frame(title: '3 things expert system', size: [1000, 500], show: true) {
                borderLayout()


                panel(constraints: BorderLayout.NORTH,
                        border: compoundBorder([emptyBorder(10), titledBorder('Knowledge base ')])) {
                    tableLayout {
                        tr {
                            td { label 'Ontology:' }
                            td {
                                textField(id: 'ontology_path', columns: 50, text = "C:/programy/protege/simplestExample.owl", actionPerformed: {
                                loadOntology();
                                })
                            }
                            td { button(text: 'Load', actionPerformed: {
                                loadOntology();
                            })
                            }
                        }
                        tr {
                            td { label 'currently loaded ontology: ' }
                            td { label(id: 'current_ontology') }
                        }
                        tr {
                            td { button(text: 'Show summary of the ontology', actionPerformed: {
                                String classResult = "";
                                Set<OWLClass> owlClassSet = ontology.getClassesInSignature(true)
                                for (OWLClass klass: owlClassSet){
                                    if (klass.getIRI().getFragment() == null){
                                        continue;
                                    }
                                    classResult = classResult + " | " +klass.getIRI().getFragment()
                                }
                                ontology_classes_summary.text = classResult

                                String individualsResult = "";
                                Set<OWLNamedIndividual> owlIndividualSet = ontology.getIndividualsInSignature(true)

                                for (OWLNamedIndividual individual: owlIndividualSet){
                                    if (individual.getIRI().getFragment() == null){
                                        continue;
                                    }
                                    individualsResult = individualsResult + " | " +individual.getIRI().getFragment()
                                }
                                ontology_individuals_summary.text = individualsResult

                                String propertyResult = "";
                                Set<OWLObjectProperty> owlPropertiesSet = ontology.getObjectPropertiesInSignature(true)
                                for (OWLObjectProperty property: owlPropertiesSet){
                                    if (property.getIRI().getFragment() == null){
                                        continue;
                                    }
                                    propertyResult = propertyResult + " | " +property.getIRI().getFragment()
                                }
                                ontology_properties_summary.text = propertyResult
                            })
                            }
                        }
                        tr {
                            td { label 'Classes summary: ' }
                            td { label(id: 'ontology_classes_summary') }
                        }
                        tr {
                            td { label 'Individuals summary: ' }
                            td { label(id: 'ontology_individuals_summary') }
                        }
                        tr {
                            td { label 'Object properties summary: ' }
                            td { label(id: 'ontology_properties_summary') }
                        }
                    }
                }

                panel(constraints: BorderLayout.CENTER,
                        border: compoundBorder([emptyBorder(10), titledBorder('What do you want to know? ')])) {
                    tableLayout {
                        tr {
                            td { label 'Please ask a question here:' }
                            td {
                                textField(id: 'question', columns: 50, text = "contains some SmallThing", actionPerformed: {
                                 askQuestion();
                                })
                            }
                            td { button(text: 'Ask', actionPerformed: {
                                askQuestion();
                            })
                            }
                        }
                        tr {
                            td { label 'The answer is: ' }
                            td { label(id: 'current_response') }
                        }


                    }
                }


            }
        }
    }

}
